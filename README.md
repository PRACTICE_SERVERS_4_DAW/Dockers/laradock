# Laradock

[![MySQL 5.7](https://img.shields.io/badge/MySQL-5.7-blue.svg)](https://github.com/docker-library/mysql/blob/fc3e856313423dc2d6a8d74cfd6b678582090fc7/5.7/Dockerfile) [![PHP 7.2](https://img.shields.io/badge/PHP-7.2-blue.svg)](https://github.com/docker-library/php/blob/b599d7375c12ff69e4d11ea10c3dea56dd670eb9/7.1/stretch/cli/Dockerfile) [![phpMyAdmin lastest](https://img.shields.io/badge/phpMyAdmin-lastest-blue.svg)](https://hub.docker.com/r/phpmyadmin/phpmyadmin/~/dockerfile/) [![ngnix alpine](https://img.shields.io/badge/nginx-alpine-blue.svg)](https://github.com/nginxinc/docker-nginx/blob/f603bb3632ea6df0bc9da2179c18eb322c286298/mainline/alpine/Dockerfile) [![node stable](https://img.shields.io/badge/node-stable-blue.svg)](https://github.com/docker-library/php/blob/b599d7375c12ff69e4d11ea10c3dea56dd670eb9/7.1/stretch/cli/Dockerfile)

## Introducción

Este repositorio pretende facilitar la creación del entorno de trabajo para el desarrollo de los proyectos de DIW basados en laravel, como por ejemplo [API_REST_Scrabble](https://gitlab.com/PRACTICE_SERVERS_4_DAW/API_REST_Scrabble).

El repositorio es un fork de [laradock](https://github.com/laradock/laradock), que se ha configurado para que funcione con las herramientas minimas necesarias y con una documentación más concisa. 

Para más información se puede consultar la documentación de laradock en [laradock.io](http://laradock.io/).

## Instalación

> La instalación requiere del uso de git.

1. Instalar Docker y Docker Compose. Puedes descargar la Community Edition (Docker CE) desde [este enlace](https://www.docker.com/community-edition#/download) (para sistemas MacOS y Windows) o seguir [estos pasos](https://docs.docker.com/install/linux/docker-ce/ubuntu/) en sistemas GNU/Linux Ubuntu. 

    > Las versiones para Windows y MacOS ya incluyen Docker Compose en la instalación. Para realizar la instalación en sistemas GNU/Linux sigue los pasos de [este enlace](https://docs.docker.com/compose/install/).

2. Clonar el repositorio. Tenemos dos opciones:
    
    A. El proyecto ya existe. Para ello hay que añadir el repositorio como submódulo al proyecto. Para ello **desde la carpeta raíz del proyecto Laravel**.

        $ git submodule add https://gitlab.com/PRACTICE_SERVERS_4_DAW/Dockers/laradock.git
         
    
    B. <a name="paso2.2"></a>El proyecto Laravel se creará a partir del contenedor. En ese caso es necesario crear una carpeta con el nombre del proyecto, por ejemplo **MiProyectoLaravel** y crear un repositorio *git*. Posteriormente ejecutar el paso 2.A.

        $ mkdir MiProyectoLaravel
        $ cd MiProyectoLaravel
        $ git init

    > Las instrucciones sobre como crear un proyecto Laravel desde los contenedores aparecen en el apartado [*Desarrollo/Laravel/Crear proyecto*](#crearLaravel).

## Arranque y parada

> Atención: en sistemas GNU/Linux es conveniente que todos los comandos que se indican se han ejecutados con _sudo_.

Desde la carpeta *MiProyectoLaravel/laradock* creamos los contenedores necesarios ejecutando: 

```
$ docker-compose up -d nginx php-fpm mysql phpmyadmin workspace_node_tools
```

La primera vez que ejecutemos el comando, la instanciación requerirá de la imagen definida en el Dockerfile, por lo tanto tendrá que descargarla de Internet, lo que le llevará más tiempo. 

> De hecho, en función de la conexión a internet y de las características del ordenador este paso podría tardar más de media hora.

Una vez realizada la descarga, las imágenes quedan almacenadas en nuestro disco duro de manera que el siguiente arranque será mucho más rápido.

Se puede comprobar que los contenedores han arrancado correctamente con el comando:

```
$ docker-compose ps 
```

Para cerrar los contenedores:

```
$ docker-compose down 
```

## Uso

### Producción

Para poder acceder al index del proyecto, desde un navegador del host accedemos a *localhost*.

```
http://localhost
```

### Desarrollo 

#### Laravel

En general, todo el trabajo de consola que se ha de realizar con Laravel se ejecuta desde el contenedor *laradock_workspace_node_tools_X* (donde X es el número de contenedor asociado a esa imagen, generalmente será 1). 

Para ello, desde el directorio *MiProyectoLaravel/laradock*, ejecutamos la aplicación *bash* del servicio *workspace_node_tools*, ya sea con *docker-compose* (que pide el nombre del servicio)

```
$ docker-compose exec workspace_node_tools bash
```
o con docker *docker-compose* (que pide el nombre del contenedor)
```
$ docker exec -i -t laradock_workspace_node_tools_1 bash
```

Ambas acciones nos abren un terminal ubicado en _/var/www_, o lo que es lo mismo, el directorio raíz del proyecto laravel. Desde él podemos ejecutar todas las órdenes habituales, por ejemplo:

**Crear un proyecto laravel**<a name="crearLaravel"></a>
```
/var/www$ composer create-project laravel/laravel --prefer-dist pry1
```

> Atención: Esta acción crea la carpeta _pry1_ (que incluye el proyecto laravel) dentro de la carpeta *MiProyectoLaravel* (o el nombre que le hayamos dado en el [paso 2.2 de la instalación](#paso2.2)). Para que no haya problemas a la hora de localizar los recursos es conveniente mover todos los ficheros de _pry1_ a la carpeta *MiProyectoLaravel* (su padre) y eliminar _pry1_. La copia la podemos realizar desde la interfaz gráfica o estando en el contenedor, situados en el reíz, ejecutar.
>```
>/var/www$ mv pry1/{.,}* .
>/var/www$ rm -d pry1 
>```
> Es MUY importante comprobar que, al realizar el desplazamiento, se hayan tenido en cuenta los ficheros ocultos (empiezan por .).

Si todo se ha realizado correctamente, debería ser posible acceder desde un navegador a la URL _http://localhost_ mostrándose la página de incio de Laravel.

**Crear una tabla de migración**
```
/var/www$ php artisan make:migration add_challenges_table --create=challenges
```

**Ejecuta la migración**
```
/var/www$ php artisan migrate
```

#### MySQL

> Los datos de acceso al SGBD son:  
> ``` 
> host: mysql 
> user: root / password: root
> user: user / password: secret
> ``` 

Todo el trabajo relacionado con la gestión del SGBD se puede realizar desde _phpMyAdmin_. Aún así, procesos más puntuales se podrían realizar desde la línea de comandos. Para ello tenemos dos opciones, usar el terminal del contenedor y desde ahí ejecutar el cliente *mysql* o ejecutar el cliente *mysql* mediante el uso de exec

Por ejemplo, para la creación de una base de datos se podría ejecutar el comando *mysql* del contenedor *laradock_mysql_1* podríamos:

```
$ docker exec -t -i laradock_mysql_1 /bin/bash
root@7ac3b69360b4$ mysql -uroot -proot
```

o

```
docker exec -i laradock_mysql_1 mysql -uroot -proot < ../database/scripts/create_database.sql
```

Donde:

* _-uroot_: se ejecuta con el usuario _root_.
* _-proot_: se utiliza como password _root_.
* _../database/scripts/create_database.sql_: ubicación del fichero sql en el que se define el script de creación de la base de datos.

#### phpMyAdmin

Conectar desde un navegador del host a *localhost:8080* con las credenciales:

> ``` 
> host: mysql 
> user: root / password: root
> ``` 

#### Sass

> Para poder utilizar las herramientas de compilación de _Sass_ es necesario instalar la primera vez los paquetes node necesarios
>```
>$ docker-compose exec workspace_node_tools bash
>/var/www$ npm install --no-audit
>```

Para compilar *Sass* se utiliza *webpack*, con el apoyo de *laravel-mix*. La ejecución se realiza desde el contenedor workspace. Por lo tanto, desde el directorio *laradock*

```
$ docker-compose exec workspace_node_tools bash
/var/www$ npm run watch-poll
```

Esta comando crea un proceso que está atento (haciendo *poll*, es decir, comprobando cada cierto tiempo) a las modificaciones realizadas en los ficheros *Sass* y automáticamente los compila a CSS.

## Referencias

* [laradock.io](http://laradock.io/)

* [Compose file version 3 reference](https://docs.docker.com/compose/compose-file/#network_mode)

* [Docker Hub. workspace_node_tools](https://hub.docker.com/r/aoltra/laradock_workspace_node_tools/)

* [How to commit changes to a docker image](https://www.techrepublic.com/article/how-to-commit-changes-to-a-docker-image/)

* [Pushing and Pulling to and from Docker Hub](https://ropenscilabs.github.io/r-docker-tutorial/04-Dockerhub.html)

## Autor

Este repositorio proviene de un fork de [laradock](https://github.com/laradock/laradock) realizado por  Alfredo Oltra (Twitter:  [@aoltra](https://twitter.com/aoltra) / [@uhurulabs](https://twitter.com/uhurulabs))


## Licencia
	
El proyecto está liberado bajo licencia [GPL 3.0](https://www.gnu.org/licenses/gpl-3.0-standalone.html).

Laradock está liberado bajo licencia [MIT](https://opensource.org/licenses/MIT).